﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableInstantiateObjectOnTouch : MonoBehaviour
{
    public GameObject cannon;
    public GameObject bulletSpawn;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<InstantiateObjectOnTouch>().enabled = false;

        if (cannon != null)
        {
            cannon.SetActive(true);
            bulletSpawn.SetActive(true);
        }
    }

    void Update()
    {
        GetComponent<InstantiateObjectOnTouch>().enabled = false;

        if (cannon != null)
        {
            cannon.SetActive(true);
            bulletSpawn.SetActive(true);
        }
    }
}
