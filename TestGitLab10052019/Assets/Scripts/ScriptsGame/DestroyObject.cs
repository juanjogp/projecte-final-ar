﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public float delayDestroy = 3.0f;
    void OnCollisionEnter(Collision col)
    {
        // Destroy object on collision with the tag prop
        // In our case the gameobjects of the structure
        if (col.gameObject.tag == "Prop")
        {
            Debug.Log("Object Destroyed");
            Destroy(col.gameObject, delayDestroy);
        }
    }
}
