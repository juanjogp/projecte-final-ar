﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBullet : MonoBehaviour
{
    public Rigidbody bullet;
    public float velocidad = 15f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonUp("Fire1"))
        {
            Rigidbody inst = Instantiate(bullet, transform.position, transform.rotation);
            inst.velocity = transform.TransformDirection(new Vector3(0, 0, velocidad));
            Physics.IgnoreCollision(inst.GetComponent<Collider>(), GetComponent<Collider>());
        }
                
    }
}
